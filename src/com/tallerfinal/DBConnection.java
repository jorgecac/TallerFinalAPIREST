package com.tallerfinal;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class DBConnection {
	private Connection connection = null;
	private String user="root";
	private String url="jdbc:mysql://localhost/taller_rest_java";
	private String pass="971623";
	
	public Connection conectar()throws ClassNotFoundException, SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			connection = (Connection) DriverManager.getConnection(url, user, pass);
			
			System.out.println("Tenemos conexion");
			
			return connection;
		}catch(SQLException sqle) {
			 sqle.printStackTrace();
			 throw sqle;
		}
		
		
	}

}
