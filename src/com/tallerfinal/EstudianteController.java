package com.tallerfinal;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class EstudianteController {

DBConnection bd;
	
	Connection conexion;
	
	public boolean agregarEstudiante(String nombre, String ci, String carrera, String facultad, int promocion) {
		
		bd = new DBConnection();
		
		try {
		
			conexion = bd.conectar();
			
			String qry = "insert into estudiantes (nombre_completo, ci, carrera, facultad, promocion) values (?,?,?,?,?)";
			
			PreparedStatement pst = (PreparedStatement) conexion.prepareStatement(qry);
			pst.setString(1, nombre);
			pst.setString(2, ci);
			pst.setString(3, carrera);
			pst.setString(4, facultad);
			pst.setInt(5, promocion);
			int result = pst.executeUpdate();
			
			pst.close();
			
			if(result > 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int eliminarEstudiante(String ci) {
		
		try {
			
			
			conexion = bd.conectar();
			System.out.println("Vamos a eliminar el estudiante con ci "+ci);
			String qry = "delete from estudiantes where ci = ?";
			
			PreparedStatement pst = (PreparedStatement) conexion.prepareStatement(qry);
			pst.setString(1, ci);
			int result = pst.executeUpdate();
			
			pst.close();
			
			return result;
		}catch(Exception sqle) {
			sqle.printStackTrace();
		}
		
		return 0;
		
		
	}
	
	public boolean modificarEstudiante(String ci, int promocion) {
		
		bd = new DBConnection();
		
		try {
			conexion = bd.conectar();
			
			String qry = "update estudiantes set promocion=?  where ci = ?";
			
			PreparedStatement pst = (PreparedStatement) conexion.prepareStatement(qry);
			
			pst.setInt(1, promocion);
			pst.setString(2, ci);
			
			int resultado = pst.executeUpdate();
			
			if(resultado > 0) {
				return true;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
		
	}
	
	
}
