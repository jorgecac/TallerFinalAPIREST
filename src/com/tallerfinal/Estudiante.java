package com.tallerfinal;

public class Estudiante {

	private int id;
	private String nombreCompleto;
	private String ci;
	private String carrera;
	private String facultad;
	private int promocion;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getCarrera() {
		return carrera;
	}
	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}
	public String getFacultad() {
		return facultad;
	}
	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}
	public int getPromocion() {
		return promocion;
	}
	public void setPromocion(int promocion) {
		this.promocion = promocion;
	}
	
	
}
