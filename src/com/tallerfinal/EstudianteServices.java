package com.tallerfinal;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

@Path("/estudiante")
public class EstudianteServices {

	
	@POST
	@Path("/agregar")
	@Consumes("application/json")
	public Response crearEstudiante(String json) {
		
		Gson jsonEstudiante = new Gson();
		
		Entidad estudiante = jsonEstudiante.fromJson(json, Entidad.class);
		
		EstudianteController controller = new EstudianteController();
		
		boolean resultado = controller.agregarEstudiante(estudiante.getNombreCompleto(), estudiante.getCi(), estudiante.getCarrera(), estudiante.getFacultad(), estudiante.getPromocion());
		
		if(resultado) {
			return Response.status(200).entity("Estudiante agregado!").build();
		}
		
		return Response.status(400).entity("No se pudo agregar al estudiante").build();
		
	}
	
	@PUT
	@Path("/actualizar")
	@Consumes("application/json")
	public Response modificarEstudiante(String json) {
		Gson jsonEstudiante = new Gson();
		
		Entidad estudiante = jsonEstudiante.fromJson(json, Entidad.class);
		
		EstudianteController controller = new EstudianteController();
		
		boolean resultado = controller.modificarEstudiante(estudiante.getCi(), estudiante.getPromocion());
		
		if(resultado) {
			return Response.status(200).entity("Estudiante modificado!").build();
		}
		
		return Response.status(400).entity("No se pudo modificar al estudiante").build();
	}
	
	
	
	@PUT
	@Path("/eliminar/{ci}")
	public Response eliminarAgregar(@PathParam("ci") int cedula) {
    	EstudianteController controller = new EstudianteController();
    	
    	String valor = Integer.toString(cedula);
    	
		int respuesta = controller.eliminarEstudiante(valor);
		if (respuesta > 0) {
			return Response.status(200).entity(respuesta+" Estudiante eliminado").build();

		}
		return Response.status(200).entity(" Ningun estudiante eliminado").build();

	}

}
